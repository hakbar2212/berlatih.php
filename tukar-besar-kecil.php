<!DOCTYPE html>
<html lang"en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, intial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
    <h1>Tukar Huruf Besar Kecil</h1>

<?php
function tukar_besar_kecil($string){

    $abjadBesar = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $output = "";
    for ($i=0; $i<strlen($string); $i++){
        $besar =strpos($abjadBesar,$string[$i]);
        if($besar == null){
            $output .= strtoupper($string[$i]);
        } else {
            $output .= strtolower($string[$i]);
        }
    
    }
    return $output."<br>";
}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>
</body>
</html>